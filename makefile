OCAMLFIND=ocamlfind
CMO=cmo
CMA=cma
OCAMLC=$(OCAMLFIND) ocamlc
# for developing purposes
LIB_DIR=../lib
#LIB_DIR=lib
INCLUDES= -I $(LIB_DIR) -I $(LIB_DIR)/dcg -I $(LIB_DIR)/ipp  # all relevant -I options here
OCAMLFLAGS= -pp "camlp4o -I $(LIB_DIR)/dcg -I $(LIB_DIR)/ipp pa_dcg.cmo pa_ipp.cmo" -w usy -thread $(INCLUDES)    # add other options for ocamlc here

SQUALL_SRC = semantics.ml syntax.ml sparql.ml dbpedia.ml
SQUALL_OBJ = $(SQUALL_SRC:.ml=.$(CMO))

COMMON_OBJ = str.$(CMA) nums.$(CMA) unix.$(CMA) common.$(CMO) lSet.$(CMO)

all: $(DIR_LIB)/common.$(CMO) $(DIR_LIB)/dcg/dcg.$(CMA) $(DIR_LIB)/ipp/ipp.$(CMA) script interpreter

$(DIR_LIB)/common.$(CMO):
	cd lib && make

$(DIR_LIB)/dcg/dcg.$(CMA):
	cd lib/dcg && make

$(DIR_LIB)/ipp/ipp.$(CMA):
	cd lib/ipp && make

script: $(SQUALL_OBJ) squall2sparql.$(CMO)
	$(OCAMLC) $(INCLUDES) -g -custom -o squall2sparql $(COMMON_OBJ) dcg.$(CMA) ipp.$(CMA) $(SQUALL_OBJ) squall2sparql.$(CMO)

interpreter: $(SQUALL_OBJ) main.$(CMO)
	$(OCAMLC) $(INCLUDES) -g -o squall.exe $(COMMON_OBJ) dcg.$(CMA) ipp.$(CMA) $(SQUALL_OBJ) main.$(CMO)

webform: webform.ml
	$(OCAMLC) $(INCLUDES) -thread -package eliom.server -c webform.ml
	$(OCAMLC) $(INCLUDES) -a -o webform.cma $(COMMON_OBJ) dcg.$(CMA) ipp.$(CMA) $(SQUALL_OBJ) webform.$(CMO)

# Common rules
.SUFFIXES: .mll .ml .mli .$(CMO) .cmi

%.$(CMO): %.ml
	$(OCAMLC) $(OCAMLFLAGS) -c $<

# Clean up
clean:
	rm -f *.cm[ioax]
	rm -f *.o

cleanall: clean
	cd lib && make clean
	cd lib/dcg && make clean
	cd lib/ipp && make clean

lisfs2008:
	install -o ocsigen -g ocsigen -m 644 webform.cma /var/lib/ocsigenserver/
	install -o ocsigen -g ocsigen -m 644 webform.css /var/lib/ocsigenserver/static/
	install -o ocsigen -g ocsigen -m 644 images/* /var/lib/ocsigenserver/static/images/

