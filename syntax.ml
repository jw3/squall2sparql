(*
    This file is part of 'squall2sparql' <http://www.irisa.fr/LIS/softwares/squall/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

open Semantics

class ['data] pre_context =
  object (self)
    val sentence_start : Msg.Coord.t = (1,1)
    method set_sentence_start coord =
      {< sentence_start = coord >}
    method is_sentence_start coord = (coord = sentence_start)

    val cpt_which : int = 1
    val stack_which : (int * 'data) list = []
    method is_top_which l =
      match stack_which with
	| [] -> false
	| (n,_)::_ -> l = Which n
    method push_which (data : 'data) =
      Which cpt_which, {< cpt_which = cpt_which+1; stack_which = (cpt_which,data)::stack_which; >}
    method pop_which =
      match stack_which with
	| [] -> []
	| (n,data)::st -> [Which n, data, {< stack_which = st; >}]

    method private is_kwd_norm s =
      List.mem s
	[ "a"; "all"; "among"; "an"; "and"; "are"; "as"; "at";
	  "be"; "between"; "by";
	  "did"; "different"; "do"; "does";
	  "else"; "equal"; "each"; "every"; "exactly"; "except";
	  "first"; "for"; "from";
	  "greater"; "greatest";
	  "had"; "has"; "have"; "higher"; "highest"; "how";
	  "if"; "in"; "into"; "is";
	  "last"; "later"; "latest"; "least"; "less"; "lesser"; "lower"; "lowest";
	  "many"; "maybe"; "more"; "most"; "much";
	  "no"; "none"; "not";
	  "of"; "often"; "one"; "only"; "or"; "other";
	  "per";
	  "same"; "second"; "share"; "shared"; "shares"; "some"; "such";
	  "than"; "that"; "the"; "then"; "there"; "third"; "times"; "to";
	  "unit"; "units";
	  "was"; "were"; "what"; "where"; "whether"; "which"; "with"; "without"; "who"; "whom"; "whose";
	]

    method is_kwd coord s =
      let s = if sentence_start = coord then String.uncapitalize s else s in
      self#is_kwd_norm s
  end
let set_sentence_start = dcg "set sentence start" [ @cursor; ?ctx; ctx' in "new ctx" [ctx#set_sentence_start cursor#coord]; !ctx' -> () ]
let is_sentence_start = dcg "is sentence start" [ @cursor; ?ctx; when "not sentence start" ctx#is_sentence_start cursor#coord -> () ]

let parse_relativize bool parse data = dcg "parse_relativize"
  [ ?ctx; l, ctx' in "failed to open relativize" [ctx#push_which data]; !ctx';
    f = parse;
    ?ctx''; when "failed to close relativize" (not (ctx''#is_top_which l)) -> (fun x -> bool#and_ [bool#label l x; f]) ]
let parse_void = dcg "parse_void"
  [ ?ctx; l, data, ctx' in "not in a relativize scope" ctx#pop_which; !ctx' -> Ref l, data ]

(* expectations *)

exception Expect of string

(* punctuations *)

let skip = dcg "skip" [ s = match "[ \t\r\n]*" as "skip" -> s ]

let ws = dcg "space" [ s = match "[ \t\r\n]+" as "space" -> s ]

let eof = dcg "eof" [ _ = skip; EOF -> () ]
let comma = dcg "comma" [ _ = skip; "," -> "," ]
let semicolon = dcg "semicolon" [ _ = skip; ";" -> ";" ]
let dot = dcg "dot" [ _ = skip; "." -> "." ]
let interro = dcg "question mark" [ _ = skip; "?" -> "?" ]
let left = dcg "left bracket" [ _ = skip; "(" -> "(" ]
let right = dcg "right bracket" [ _ = skip; ")" -> ")" ]
let left_square = dcg "left square bracket" [ _ = skip; "[" -> "[" ]
let right_square = dcg "right square bracket" [ _ = skip; "]" -> "]" ]
let left_curly = dcg "left curly bracket" [ _ = skip; "{" -> "{" ]
let right_curly = dcg "right curly bracket" [ _ = skip; "}" -> "}" ]
let bar = dcg "bar" [ _ = skip; "|" -> "|" ]
let underscore = dcg "underscore" [ _ = skip; "_" -> "_" ]
let ellipsis = dcg "ellipsis" [ _ = skip; "..." -> "..." ]
let equal = dcg "equal" [ _ = skip; "=" -> "=" ]

(* grammatical morphemes and words *)

let kwds_bare lw = dcg "kwds"
  [ @cursor; ?ctx; s = match "[A-Za-z]+";
    when "" (List.mem s lw || (ctx#is_sentence_start cursor#coord && List.mem (String.uncapitalize s) lw)) -> s
  | exception (Expect (String.concat "/" lw)) ]
let kwds lw = dcg [ _ = skip; s = kwds_bare lw -> s ]

let kwd_bare w = kwds_bare [w]
let kwd w = kwds [w]

let kwds_does = ["does"; "did"; "do"]
let kwds_be = ["is"; "are"; "were"; "was"; "be"]
let kwds_have = ["has"; "have"; "had"]
let kwds_share = ["share"; "shares"; "shared"]

let a_an = dcg [ s = kwds ["a"; "an"] -> s ]
let a_the = dcg [ s = kwds ["a"; "an"; "the"] -> s ]
let does = dcg [ s = kwds kwds_does -> s ]
let be = dcg [ s = kwds kwds_be -> s ]
let have = dcg [ s = kwds kwds_have -> s ]
let share = dcg [ s = kwds kwds_share -> s ]
let what = dcg [ s = kwds ["what"; "who"; "whom"] -> s ]

(* words *)
let re_word_aux = "[a-zA-Z_0-9.']*[a-zA-Z_0-9]"
let re_word = "[a-zA-Z]" ^ re_word_aux
let re_word_lowercase = "[a-z]" ^ re_word_aux
let re_word_uppercase = "[A-Z]" ^ re_word_aux

(* URIs *)
let re_reluri = "<[^<>\"{}|^`\\\x00-\x20]*>"
let re_prefix = "\\([A-Za-z]\\([A-Za-z_0-9.-]*[A-Za-z_0-9-]\\)?\\)?:"
let re_local = "[A-Za-z_:0-9]\\([A-Za-z_0-9-.:]*[A-Za-z_0-9-:]\\)?"
let re_qname = re_prefix ^ re_local
let re_qname_quoted = re_prefix ^ "<\\(" ^ re_local ^ "\\)>"
let re_qname_word re_word = re_prefix ^ re_word

let parse_uri_bare re_word = dcg "uri"
  [ rel = match re_reluri as "relative URI" -> rel
  | qq = match re_qname_quoted as "quoted qname" -> Str.global_replace (Str.regexp "[<>]") "" qq
  | qw = match re_qname_word re_word as "qualified word" -> qw
  | @cursor; ?ctx;
    word = match re_word as "bare word";
    when '("reserved keyword: " ^ word) not (ctx#is_kwd cursor#coord word) -> ":" ^ word
  | exception (Expect "<URI>") ]
let parse_uri re_word = dcg [ _ = skip; uri = parse_uri_bare re_word -> uri ]

let parse_nat_bare = dcg "nat"
  [ s = match "\\(0\\|[1-9][0-9]*\\)" as "natural number" -> int_of_string s
  | exception (Expect "<natural number>") ]
let parse_nat = dcg [ _ = skip; n = parse_nat_bare -> n ]
let parse_literal_nat = dcg [ n = parse_nat -> Literal (string_of_int n) ]

let parse_ordinal_bare = dcg "ordinal"
    [ _ = kwd_bare "first" -> 1
    | _ = kwd_bare "second" -> 2
    | _ = kwd_bare "third" -> 3
    | n = parse_nat_bare; _ = match "\\(st\\|nd\\|rd\\|th\\)" as "th" -> n
    | exception (Expect "<ordinal>") ]
let parse_ordinal = dcg [ _ = skip; n = parse_ordinal_bare -> n ]

let parse_percentage_bare = dcg "percentage"
  [ s = match "\\(0\\|[1-9][0-9]\\|100\\)%" as "percentage" ->
    let s1 = String.sub s 0 (String.length s - 1) in
    let n = int_of_string s1 in
    Literal (string_of_int (n/100) ^ "." ^ string_of_int (n mod 100))
  | exception (Expect "<percentage>") ]
let parse_percentage = dcg [ _ = skip; p = parse_percentage_bare -> p ]

let re_date = "[0-9][0-9][0-9][0-9]-\\(0[1-9]\\|1[0-2]\\)-\\(0[1-9]\\|1[0-9]\\|2[0-9]\\|3[0-1]\\)"
let re_time = "\\([0-1][0-9]\\|2[0-3]\\):[0-5][0-9]:[0-5][0-9]\\(\\.[0-9]+\\)?"
let re_dateTime = re_date ^ "T" ^ re_time

let parse_string_bare = dcg
    [ s = match "\"\\([^\"\\\n\r]\\|[\\][tbnrf\"]\\)*\"" as "string" -> s
    | s = match "\"\"\"\\(\\(\"\\|\"\"\\)?\\([^\"\\]\\|[\\][tbnrf\"]\\)\\)*\"\"\"" as "long string" -> s
    | exception (Expect "<string>") ]
let parse_string = dcg [ _ = skip; s = parse_string_bare -> s ]

let parse_literal_str_bare = dcg "literal_str"
    [ _ = kwd_bare "true" -> "true"
    | _ = kwd_bare "false" -> "false"
    | _ = kwd_bare "the"; uri = parse_uri re_word; s = parse_string -> s ^ "^^" ^ uri
    | s = match re_dateTime as "dateTime" -> "\"" ^ s ^ "\"^^xsd:dateTime" 
    | s = match re_date as "date" -> "\"" ^ s ^ "\"^^xsd:date"
    | s = match re_time as "time" -> "\"" ^ s ^ "\"^^xsd:time"
    | s = match "[+-]?\\([0-9]+\\(\\.[0-9]*\\)?\\|\\.[0-9]+\\)[eE][+-]?[0-9]+" as "double" -> "\"" ^ s ^ "\"^^xsd:double"
    | s = match "[+-]?[0-9]*\\.[0-9]+" as "decimal" -> "\"" ^ s ^ "\"^^xsd:decimal"
    | s = match "[+-]?[0-9]+" as "integer" -> s
    | s = parse_string_bare;
      ( "@"; lang = match "[-a-zA-Z0-9]+" as "language tag" -> s ^ "@" ^ lang
      | "^^"; uri = parse_uri_bare re_word -> s ^ "^^" ^ uri
      |  -> s )
    | exception (Expect "<literal>") ]
let parse_literal = dcg "literal" [ _ = skip; s = parse_literal_str_bare -> Literal s ]

let parse_pattern_bare = dcg "pattern"
    [ re = match "'[^\n']+'" as "pattern";
      lre in "invalid pattern" [Str.split (Str.regexp "[ \t']+") re]; when "empty pattern" lre <> [] -> matches lre
    | exception (Expect "<pattern>") ]
let parse_pattern = dcg [ _ = skip; pat = parse_pattern_bare -> pat ]

let re_user_var = "[a-zA-Z0-9]+"
let re_user_var_long = "[?]" ^ re_user_var
let parse_user_var_bare = dcg "var"
  [ s = match re_user_var_long as "long user variable" -> UserVar s
  | s = match re_user_var as "user variable"; when "invalid short user variable" (Str.string_match (Str.regexp "[A-Z][0-9]*$") s 0) -> UserVar s
  | exception (Expect "<user var>") ]
let parse_user_var = dcg [ _ = skip; v = parse_user_var_bare -> v ]
let parse_ref = dcg "ref" [ l = parse_user_var -> Ref l ]

(* Lexicon *)

let belongs x = get_arg "to" "The verb 'belong' expects the preposition 'to'" (fun c -> a c x)
let relates p x = get_arg "to" "The verb 'relate' expects the preposition 'to'" (fun y -> rel p x y)

let rec parse_e_default = dcg
  [ _ = kwd "the"; _ = kwd "default"; _ = kwd "graph" -> Uri "DEFAULT"
  | _ = kwd "all"; _ = kwd "named"; _ = kwd "graphs" -> Uri "NAMED"
  | _ = kwd "all"; _ = kwd "graphs" -> Uri "ALL"
  | _ = kwd "the"; _ = kwds ["resource"; "class"; "property"]; uri = parse_uri re_local -> Uri uri
  | uri = parse_uri re_word_uppercase -> Uri uri ]

let rec parse_p1_default = dcg
    [ uri = parse_uri re_word -> a (Uri uri) ]

let rec parse_p2_default = dcg
    [ uri = parse_uri re_word_lowercase; suf = parse_p2_suffix_opt -> rel (Uri (suf uri)) ]
and parse_p2_suffix_opt = dcg
    [ s = match "[?+*]" as "property suffix" -> (fun uri -> uri ^ s)
    | s = match "{[0-9]+\\(,\\([0-9]+\\)?\\)?}" as "property suffix" -> (fun uri -> uri ^ s)
    | s = match "{,[0-9]+}" as "property suffix" -> (fun uri -> uri ^ s)
    |  -> (fun uri -> uri) ]

class ['data,'s1,'p1,'pn,'p2,'p2_measure,'str] context =
object (self)
  inherit ['data] pre_context as super

  method parse_e_np : 's1 = dcg
    [ x = parse_e_default -> x
    | exception (Expect "<entity>") ]

  method parse_p1_noun : 'p1 = dcg
    [ _ = kwds ["thing"; "things"] -> bool1#true_
    | p1 = parse_p1_default; _ = match "\\(-e?s\\)?" as "plural mark" -> p1
    | exception (Expect "<noun>") ]
  method parse_p1_adj : 'p1 = dcg
    [ _ = kwd "belonging" -> belongs
    | _ = kwd "related" -> passive relates
    | p2 = parse_p2_default; "-ed" -> passive p2
    | exception (Expect "<adjective>") ]
  method parse_p1_verb : 'p1 = dcg
    [ _ = kwds ["exist"; "exists"; "existed"] -> bool1#true_
    | _ = kwds ["belong"; "belongs"; "belonged"] -> belongs ]
  (*    | p1 = parse_p1_default -> p1 | exception (Expect "intransitive verb") ] *)
  method parse_pn_imp : 'pn = dcg
    [ _ = kwd "return" -> proc proc_return
    | _ = kwd "list" -> proc proc_return
    | _ = kwd "give"; _ = kwd "me" -> proc proc_return ]

  method parse_p2_noun : 'p2 = dcg
    [ _ = kwd "number"; _ = kwd "of"; p2 = self#parse_p2_count_noun -> p2
    | p2 = parse_p2_default; _ = match "\\(-e?s\\)?" as "plural mark" -> p2
    | exception (Expect "<relation noun>") ]
  method parse_p2_count_noun : 'p2 = Dcg.fail
  method parse_p2_adj : 'p2 = dcg
    [ _ = kwd "relating" -> relates
    | p2 = parse_p2_default; "-ing" -> p2
    | exception (Expect "<transitive adjective>") ]
  method parse_p2_adj_measure : 'p2_measure = Dcg.fail
  method parse_p2_adj_comparative : 'p2_measure = Dcg.fail
  method parse_p2_adj_superlative : 'p2_measure = Dcg.fail
  method parse_p2_verb : 'p2 = dcg
    [ _ = kwds ["relate"; "relates"; "related"] -> relates
    | _ = be; "-"; p2 = parse_p2_default -> p2
    | p2 = parse_p2_default; _ = match "\\(-e?[sd]\\)?" as "verb suffix" -> p2
    | exception (Expect "<transitive verb>") ]

  method parse_marker : 'str = dcg
    [ _ = kwd "to" -> "to"
    | _ = kwd "into" -> "into"
    | _ = kwd "by" -> "by"
    | _ = kwd "at" -> "at" ]

  method is_kwd_norm s =
    super#is_kwd_norm s ||
    List.mem s
      [ "belong"; "belonged"; "belongs"; "belonging";
	"class";
	"default";
	"exist"; "existed"; "exists";
	"give"; "graphs";
	"list";
	"me";
	"named";
	"property";
	"relate"; "related"; "relates"; "relating"; "resource"; "return";
	"thing"; "things";
      ]
end

(* Brackets *)

let parse_brackets parse_comp = dcg
  [ _ = left; x = parse_comp; _ = right -> x ]

(* Boolean closure *)

let rec parse_bool bool parse_atom = dcg
    [ la = LIST1 parse_and bool parse_atom SEP [ _ = kwd "or" -> () ] ->
        match la with [a] -> a | _ -> bool#or_ la ]
and parse_and bool parse_atom = dcg
    [ la = LIST1 parse_maybe bool parse_atom SEP [ _ = kwd "and" -> () ] ->
        match la with [a] -> a | _ -> bool#and_ la ]
and parse_maybe bool parse_atom = dcg
    [ _ = kwd "maybe"; a1 = parse_not bool parse_atom -> bool#maybe_ a1
    | _ = kwd "if"; _ = kwd "defined"; _ = comma; a1 = parse_not bool parse_atom -> bool#maybe_ a1
    | a1 = parse_not bool parse_atom -> a1 ]
and parse_not bool parse_atom = dcg
    [ _ = kwd "not"; a = parse_atom -> bool#not_ a
(*    | _ = left; a = parse_bool bool parse_atom; _ = right -> a *)
    | a = parse_atom -> a ]

(* Expressions *)

let rec parse_expr parse_s bool parse_atom = dcg
    [ e = parse_cond parse_s bool parse_atom -> e ]
and parse_cond parse_s bool parse_atom = dcg
  [ e1 = parse_add parse_s bool parse_atom;
    ( _ = comma; _ = kwd "if"; s = parse_s; _ = comma; _ = kwd "else"; e2 = parse_cond parse_s bool parse_atom ->
      (fun k -> bool#the (bool#truth s) (fun c -> bool#cond c (e1 k) (e2 k)))
(*      (fun k -> e1 (fun x1 -> e2 (fun x2 -> bool#the (bool#func func_if [truth s; x1; x2]) k))) *)
    |  -> e1 ) ]
and parse_add parse_s bool parse_atom = dcg
    [ e1 = parse_mult parse_s bool parse_atom; e = parse_add_aux parse_s bool parse_atom e1 -> e ]
and parse_add_aux parse_s bool parse_atom e1 = dcg
    [ op = parse_addop; e2 = parse_mult parse_s bool parse_atom;
      e = parse_add_aux parse_s bool parse_atom
	(fun k -> e1 (fun x1 -> e2 (fun x2 -> bool#the (bool#func op [x1; x2]) k)))
	-> e
    |  -> e1 ]
and parse_mult parse_s bool parse_atom = dcg
    [ e1 = parse_unary parse_s bool parse_atom; e = parse_mult_aux parse_s bool parse_atom e1 -> e ]
and parse_mult_aux parse_s bool parse_atom e1 = dcg
    [ op = parse_mulop; e2 = parse_unary parse_s bool parse_atom;
      e = parse_mult_aux parse_s bool parse_atom
	(fun k -> e1 (fun x1 -> e2 (fun x2 -> bool#the (bool#func op [x1; x2]) k)))
	-> e
    |  -> e1 ]
and parse_unary parse_s bool parse_atom = dcg
    [ lit = parse_literal -> (fun k -> k lit) (* redundant with parse_primary for negative numbers *)
    | op = parse_unop; e1 = parse_primary parse_s bool parse_atom ->
        (fun k -> e1 (fun x1 -> bool#the (bool#func op [x1]) k))
    | e1 = parse_primary parse_s bool parse_atom -> e1 ]
and parse_primary parse_s bool parse_atom = dcg
(*    [ _ = left; e = parse_cond parse_s bool parse_atom; _ = right -> e *)
    [ lit = parse_literal -> (fun k -> k lit)
    | l = parse_user_var; _ = equal; e = parse_cond parse_s bool parse_atom ->
        (fun k -> e (fun x1 -> bool#and_ [bool#label l x1; k x1]))
(*      |  -> (fun k -> k (Ref l)) ) *) (* redundant *)
    | op = parse_nulop -> (fun k -> bool#the (bool#func op []) k)
    | op = parse_func; _ = left; f = parse_args parse_s bool parse_atom; _ = right -> f op
    | e = parse_atom -> e ]
and parse_args parse_s bool parse_atom = dcg
    [ e1 = parse_cond parse_s bool parse_atom; f = parse_args_aux parse_s bool parse_atom ->
        (fun op k -> e1 (fun x1 -> f op [x1] k))
    |  -> (fun op k -> bool#the (bool#func op []) k) ]
and parse_args_aux parse_s bool parse_atom = dcg
    [ _ = comma; ei = parse_cond parse_s bool parse_atom; f = parse_args_aux parse_s bool parse_atom ->
        (fun op lx k -> ei (fun xi -> f op (lx@[xi]) k))
    |  -> (fun op lx k -> bool#the (bool#func op lx) k) ]
and parse_addop = dcg [ ?ctx; op = ctx#parse_addop -> op ]
and parse_mulop = dcg [ ?ctx; op = ctx#parse_mulop -> op ]
and parse_unop = dcg [ ?ctx; op = ctx#parse_unop -> op ]
and parse_nulop = dcg [ ?ctx; op = ctx#parse_nulop -> op ]
and parse_func = dcg [ ?ctx; op = ctx#parse_func -> op ]

(* Enumerations *)

let rec parse_enum bool parse_atom = dcg "enum"
  [ x1 = parse_atom;
    ( x2 = parse_enum_aux bool parse_atom -> bool#or_ [x1; x2]
    |  -> x1 ) ]
and parse_enum_aux bool parse_atom = dcg
  [ _ = comma;
    ( _ = kwd "and"; x2 = parse_atom -> x2
    | x1 = parse_atom; x2 = parse_enum_aux bool parse_atom -> bool#or_ [x1; x2] ) ]

(* Sequences *)

let rec parse_seq parse_np = dcg "parse_seq"
  [ np = parse_np;
    ( npl = parse_seq_aux parse_np -> (fun dl -> np (fun x -> npl (fun l -> dl (x::l))))
    |  -> (fun dl -> np (fun x -> dl [x])) ) ]
and parse_seq_aux parse_np = dcg
  [ _ = comma;
    ( _ = kwd "and"; np = parse_np -> (fun dl -> np (fun x -> dl [x]))
    | np = parse_np; npl = parse_seq_aux parse_np -> (fun dl -> np (fun x -> npl (fun l -> dl (x::l)))) ) ]

(* Comparisons *)

let parse_comp_det parse_x = dcg
  [ x = parse_x -> pred2_eq, x
  | _ = kwd "exactly"; x = parse_x -> pred2_eq, x
  | _ = kwd "at";
    ( _ = kwd "least"; x = parse_x -> pred2_geq, x
    | _ = kwd "most"; x = parse_x -> pred2_leq, x )
  | _ = kwd "more"; _ = kwd "than"; x = parse_x -> pred2_gt, x
  | _ = kwd "less"; _ = kwd "than"; x = parse_x -> pred2_lt, x ]

let parse_comp_between parse_x = dcg
  [ _ = kwd "between"; x1 = parse_x; _ = kwd "and"; x2 = parse_x -> x1, x2 ]

let parse_comp_np parse_x parse_y = dcg
  [ _ = kwd "more"; x = parse_x; _ = kwd "than"; y = parse_y -> pred2_gt, x, y
  | _ = kwd "less"; x = parse_x; _ = kwd "than"; y = parse_y -> pred2_lt, x, y
  | _ = kwd "as"; _ = kwd "many"; x = parse_x; _ = kwd "as"; y = parse_y -> pred2_eq, x, y ]

let parse_comp_p2_aux parse_x parse_y = dcg
  [ _ = kwd "same"; x = parse_x; _ = kwd "as"; y = parse_y -> pred2_eq, x, y
  | _ = kwd "equal"; x = parse_x; _ = kwd "to"; y = parse_y -> pred2_eq, x, y
  | _ = kwd "other"; x = parse_x; _ = kwd "than"; y = parse_y -> pred2_neq, x, y
  | _ = kwd "different"; x = parse_x; _ = kwd "from"; y = parse_y -> pred2_neq, x, y
  | _ = kwds ["greater"; "higher"; "later"];
    ( _ = kwd "or"; _ = kwd "equal"; x = parse_x; _ = kwd "to"; y = parse_y -> pred2_geq, x, y
    | x = parse_x; _ = kwd "than"; y = parse_y -> pred2_gt, x, y )
  | _ = kwds ["lesser"; "lower"; "earlier"];
    ( _ = kwd "or"; _ = kwd "equal"; x = parse_x; _ = kwd "to"; y = parse_y -> pred2_leq, x, y
    | x = parse_x; _ = kwd "than"; y = parse_y -> pred2_lt, x, y ) ]
let parse_comp_p2_binary parse_x parse_y = dcg
  [ cmp,x,y = parse_comp_p2_aux parse_x parse_y -> cmp, x, y ]
let parse_comp_p2_unary parse_y = dcg
  [ cmp, _, y = parse_comp_p2_aux (dcg [ -> () ]) parse_y -> cmp, y ]


let kcomp1 comp1 d = exists (fun n -> bool0#and_ [d n; comp1 n])
let kcomp2 comp2 d1 d2 = exists (fun n1 -> exists (fun n2 -> bool0#and_ [d1 n1; d2 n2; comp2 n1 n2]))

(* syntactical constructs *)
let rec parse = dcg
    [ f = parse_text; _ = eof -> f ]

and parse_text = dcg
    [ lf = LIST1 parse_s SEP ws -> and_ lf ]

and parse_s = dcg
    [ _ = set_sentence_start; s = parse_s_whether;
      ( _ = dot -> tell s
      | _ = interro -> tell (whether s) ) ]
and parse_s_whether = dcg
    [ _ = kwd "whether"; s = parse_s_for -> whether s
    | s = parse_s_for -> s ]
and parse_s_for = dcg
    [ _ = kwd "if"; s1 = parse_s_for; _ = [ _ = comma -> () | _ = kwd "then" -> () ];
      s2 = parse_s_for -> implies s1 s2
(*
      ( _ = kwd "else"; s3 = parse_s_for -> ifthenelse s1 s2 s3
      |  -> ifthen s1 s2 )
*)
    | _ = kwd "for"; np = parse_np; _ = comma; s = parse_s_for -> np (fun x -> s)
    | _ = kwd "there"; _ = be; np = parse_np_gen parse_det_unary -> np (fun x -> bool0#true_)
    | s = parse_s_where -> s ]
and parse_s_where = dcg
    [ s1 = parse_s_pp;
      ( _ = kwd "where"; s2 = parse_s_for -> where s1 s2
      |  -> s1 ) ]
and parse_s_pp = dcg
    [ pp = parse_pp; s = parse_s_pp -> pp s
    | s = parse_s_bool -> s ]
and parse_s_bool = dcg
    [ s = parse_bool bool0 parse_s_atom -> s ]
and parse_s_atom = dcg
    [ s = parse_brackets parse_s_for -> s
    | _ = kwd "how"; pol, p2 = parse_p2_adj_measure; _ = be; np = parse_np ->
        np (fun x -> which bool1#true_ (p2 x))
    | _ = does; np = parse_np;
      ( vp = parse_vp_atom -> whether (np vp)
      | _ = kwd "have"; vp = parse_vp_have -> whether (np vp) )
    | _ = have; np = parse_np; vp = parse_vp_have -> whether (np vp)
    | _ = be;
      ( np = parse_np; vp = parse_vp_be -> whether (np vp)
      | _ = kwd "there"; np = parse_np_gen parse_det_unary -> whether (np bool1#true_) )
    | p1 = parse_p1_imp; op = parse_op -> op p1
    | pn = parse_pn_imp; opl = parse_op_seq -> opl pn
    | np = parse_np; vp = parse_vp -> np vp
    | p1 = parse_npq_gen ["which"]; d = parse_relativize bool0 parse_s_for `None -> which p1 d
    | p2 = parse_npq2_gen ["which"]; rel_opt = parse_rel_opt; d = parse_relativize bool0 parse_s_for (`Rel p2) -> which (rel_opt bool1#true_) d ]

and parse_np_gen parse_det = dcg
    [ lnp = LIST1 parse_np_gen_bool parse_det SEP comma -> bool1#and_ lnp ]
and parse_np_gen_bool parse_det = dcg
    [ np = parse_bool bool1 (parse_np_gen_expr parse_det) -> np ]
and parse_np_gen_expr parse_det = dcg
    [ e1 = parse_expr parse_s_for bool1
	(dcg [ np = parse_np_gen_atom parse_det -> (fun k1 d -> np (fun x -> k1 x d)) ]) ->
	  e1 (fun x1 d -> d x1) ]
and parse_np_gen_atom parse_det = dcg
    [ np = parse_brackets (parse_np_gen_bool parse_det) -> np
    | t = parse_term -> (fun d -> d t)
    | np = parse_np_gen_term (parse_np_gen_bool parse_det) -> np
    | np2 = parse_np2_gen parse_det; _ = kwd "of"; np = parse_np_void ->
        (fun d -> np (fun x -> np2 x d))
    | det = parse_det; p1 = parse_ng1 ->
        (fun d -> det (close_modif (init p1)) d)
    | detenum = parse_det_enum; npq = parse_npq_enum ->
        (fun d -> detenum npq d)
    | detn = parse_det_numeric; p1c = parse_ng1_count ->
        (fun d -> detn pol_positive (p1c d))
    | op, p1c1, p1c2 = parse_comp_np parse_ng1_count parse_ng1_count ->
        (fun d -> kcomp2 (pred2 op) (p1c1 d) (p1c2 d))
    | d1 = parse_npq_gen ["which"] -> (fun d -> which d1 d) ]
(* ambiguous with relative 'whose' *)
(*
    | _ = kwd "whose"; p2 = parse_ng2 -> (* = the NG2 of what *)
	(fun d ->
	  which bool1#true_ (fun x ->
	    exists (bool1#and_ [init (p2 x); d]))) ]
*)

and parse_np2_gen parse_det = dcg
    [ np2 = parse_bool bool2 (parse_np2_gen_expr parse_det) -> np2 ]
and parse_np2_gen_expr parse_det = dcg
    [ e1 = parse_expr parse_s_for bool2
	(dcg [ np2 = parse_np2_gen_atom parse_det ->
	  (fun k1 x d -> np2 x (fun y -> k1 y x d)) ]) ->
	    e1 (fun x1 x d -> d x1) ]
and parse_np2_gen_atom parse_det = dcg
    [ np2 = parse_brackets (parse_np2_gen parse_det) -> np2
    | detn = parse_det_numeric; p2c = parse_ng2_count ->
        (fun x d -> detn pol_positive (p2c x d))
    | op, p2c1, p2c2 = parse_comp_np parse_ng2_count parse_ng2_count ->
        (fun x d -> kcomp2 (pred2 op) (p2c1 x d) (p2c2 x d))
    | op, p2c, np = parse_comp_np parse_ng2_count parse_np ->
        (fun x1 d -> np (fun x2 -> bool0#and_ [diff x1 x2; kcomp2 (pred2 op) (p2c x1 d) (p2c x2 d)]))
    | det = parse_det; p2 = parse_ng2 ->
        (fun x d -> det (init (p2 x)) d) (* close_modif ? *)
    | detenum = parse_det_enum; npq2 = parse_npq2_enum ->
        (fun x d -> detenum (npq2 x) d)
    | p2 = parse_npq2_gen ["which"] ->
        (fun x d -> which d (init (p2 x))) ]

and parse_npq_gen kwds_det = dcg
    [ npq = parse_npq_gen_atom kwds_det -> npq
    | e1 = parse_expr parse_s_for bool0
	(dcg [ d = parse_npq_gen_atom kwds_det ->
	  (fun k0 -> exists (bool1#and_ [d; k0])) ]) ->
        (fun x -> e1 (fun x1 -> unify x1 x)) ]
and parse_npq_gen_atom kwds_det = dcg (* TODO: if kwds_det = which, then 'which' must be used *)
    [ npq = parse_brackets (parse_npq_gen kwds_det) -> npq
    | t = parse_term -> (fun x -> unify t x)
    | np = parse_np_gen_term
	(dcg [ d1 = parse_npq_gen kwds_det -> (fun d -> exists (bool1#and_ [d1; d])) ]) ->
        (fun x -> np (fun x1 -> unify x1 x))
    | p2 = parse_npq2_gen_atom kwds_det; _ = kwd "of"; np = parse_np_void ->
        (fun y -> np (fun x -> p2 x y))
    | _ = kwds kwds_det; p1 = parse_ng1 -> p1
    | when "npq determiner should be which" kwds_det = ["which"]; _ = what -> bool1#true_ (* = which thing *) ]
and parse_npq_enum = dcg
  [ npq = parse_enum bool1 (dcg [ npq = parse_npq_gen ["the"] -> close_modif (init npq) ]) -> npq ]

and parse_npq2_gen kwds_det = dcg
    [ npq2 = parse_npq2_gen_atom kwds_det -> npq2
    | e1 = parse_expr parse_s_for bool1
	(dcg [ p2 = parse_npq2_gen_atom kwds_det ->
	  (fun k1 x -> exists (fun y -> bool0#and_ [p2 x y; k1 y x])) ]) ->
	(fun x y -> e1 (fun y1 x -> unify y1 y) x) ]
and parse_npq2_gen_atom kwds_det = dcg
    [ npq2 = parse_brackets (parse_npq2_gen kwds_det) -> npq2
    | _ = kwds kwds_det; p2 = parse_ng2 -> p2 ]
and parse_npq2_enum = dcg
  [ npq2 = parse_enum bool2 (dcg [ npq2 = parse_npq2_gen ["the"] -> (fun x -> close_modif (init (npq2 x))) ]) -> npq2 ]

and parse_np_gen_term parse_np = dcg "np_term"
    [ _ = kwd "that"; s = parse_s_pp -> (fun d -> graph_literal s d)
    | _ = kwd "whether"; s = parse_s_for -> (fun d -> bool0#the (truth s) d)
    | _ = underscore (* joker *) -> (fun d -> exists d)
    | _ = left_square; npl = parse_np_gen_list parse_np; _ = right_square -> npl
    | b = parse_b -> (fun d -> exists (bool1#and_ [b; d])) (* Turtle blank node: for Turtle compatibility *)
    | patt = parse_pattern -> (fun d -> exists (bool1#and_ [d; patt])) ]
and parse_np_gen_list parse_np = dcg "np_list"
    [ np = parse_np;
      ( _ = comma; npl = parse_np_gen_list parse_np ->
	(fun dl -> np (fun e -> npl (fun l -> dl (cons e l))))
      | _ = bar; npl = parse_np ->
	(fun dl -> np (fun e -> npl (fun l -> dl (cons e l))))
      |   ->
	(fun dl -> np (fun x -> dl (cons x nil))) )
    | _ = ellipsis;
      ( _ = comma; npl = parse_np_gen_list parse_np ->
	(fun dl -> npl (fun l -> exists (fun x -> bool0#and_ [triple x (Uri prop_sublist) l; dl x])))
      | _ = bar; npl = parse_np ->
	(fun dl -> npl (fun l -> exists (fun x -> bool0#and_ [triple x (Uri prop_sublist) l; dl x])))
      |  ->
        (fun dl -> exists dl) ) ]

and parse_term = dcg
    [ r = parse_ref -> r
    | lit = parse_literal -> lit
    | _ = left_square; _ = right_square -> nil
    | x = parse_e_np -> x ]

and parse_b = dcg
    [ _ = left_square; vp = OPT parse_vp ELSE bool1#true_; _ = right_square -> init vp ]

and parse_det_numeric = dcg
    [ op, n = parse_comp_det parse_literal ->
        (fun pol d -> exists (fun x -> bool0#and_ [d x; pred2 (pol op) x n]))
    | n1, n2 = parse_comp_between parse_literal ->
        (fun pol d -> exists (fun x -> bool0#and_ [d x; pred2 pred2_geq x n1; pred2 pred2_leq x n2]))
    | _ = kwd "how"; _ = kwds ["many"; "much"] ->
	(fun pol d -> which bool1#true_ d)
    | _ = kwd "the"; op_modif = parse_offset_limit_opt;
	( _ = kwd "most" ->
	  (fun pol d -> exists (fun x -> bool0#and_ [d x; open_modif (pol (`Mod { op_modif with order=`DESC })) [] x bool0#true_]))
        | _ = kwd "least" ->
	  (fun pol d -> exists (fun x -> bool0#and_ [d x; open_modif (pol (`Mod { op_modif with order=`ASC })) [] x bool0#true_])) ) ]

and parse_det_unary = dcg
    [ det1 = parse_det_unary_aux -> (fun d1 d2 -> det1 (bool1#and_ [d1; d2])) ]
and parse_det_unary_aux = dcg
    [ _ = kwd "some" -> (fun d -> exists d)
    | _ = kwd "no" -> (fun d -> bool0#not_ (exists d))
    | detn = parse_det_numeric -> (fun d -> detn pol_positive (a_number d))
    | _ = kwds ["a"; "an"];
      ( m1 = parse_modif_opt;
	( _ = kwd "number"; app_opt = parse_app_opt; rel_opt = parse_rel_opt; _ = kwd "of" ->
	  (fun d -> exists (bool1#and_ [a_number d; m1; app_opt (rel_opt bool1#true_)]))
	| op = parse_aggreg_adj; app_opt = parse_app_opt; rel_opt = parse_rel_opt ->
	  (fun d -> exists (bool1#and_ [aggreg1 op 0 (fun y lz -> d y); m1; app_opt (rel_opt bool1#true_)])) )
      |  -> (fun d -> exists d) ) ]

and parse_det = dcg
    [ det = parse_det_unary -> det
    | _ = kwds ["every"; "all"] -> (fun d1 d2 -> forall d1 d2)
    | _ = kwd "the" -> (fun d1 d2 -> the d1 d2)
    | op, pc = parse_comp_det parse_percentage; _ = kwd "of" ->
      (fun d1 d2 -> how_many_of d1 d2 (fun x -> pred2 op x pc))
    | pc1, pc2 = parse_comp_between parse_percentage; _ = kwd "of" ->
      (fun d1 d2 -> how_many_of d1 d2 (fun x -> bool0#and_ [pred2 pred2_geq x pc1; pred2 pred2_leq x pc2])) ]

and parse_det_enum = dcg "det_enum"
  [ _ = kwd "one"; _ = kwd "of" -> (fun d1 d2 -> exists (bool1#and_ [d1; d2]))
  | _ = kwd "each"; _ = kwd "of" -> (fun d1 d2 -> forall d1 d2)
  | _ = kwd "none"; _ = kwd "of" -> (fun d1 d2 -> bool0#not_ (exists (bool1#and_ [d1; d2])))
  | _ = kwd "only" -> (fun d1 d2 -> forall d2 d1) ]

and parse_np = dcg
    [ np = parse_np_gen parse_det -> np ]

and parse_np_void = dcg
  [ np = parse_np -> np
  | r, _ = parse_void -> (fun d -> d r) ]

and parse_np2 = dcg
    [ np2 = parse_np2_gen parse_det -> np2 ]

and parse_np2_void = dcg
  [ np2 = parse_np2 -> np2
  | r, data = parse_void; p2 in "not a P2 relativize" (match data with `Rel p2 -> [p2] | _ -> []) -> (* maybe a bug in DCG *)
    (fun x d -> bool0#and_ [p2 x r; d r]) ]

and parse_ng1 = dcg
    [ m1 = parse_modif_opt; p1 = parse_ng1_atom -> bool1#and_ [m1; p1] ]
and parse_ng1_atom = dcg
    [ adj_opt = parse_p1_adj_opt; l = parse_user_var; rel_opt = parse_rel_opt -> rel_opt (adj_opt (label l))
    | adj_opt = parse_p1_adj_opt; p1 = parse_p1_noun; app_opt = parse_app_opt; rel_opt = parse_rel_opt ->
        app_opt (rel_opt (adj_opt p1))
    | op = parse_aggreg_noun; app_opt = parse_app_opt; n,g = parse_g'_noun -> app_opt (aggreg1 op n g)
    | _ = kwd "number"; app_opt = parse_app_opt; rel_opt = parse_rel_opt; _ = kwd "of"; n,g = parse_g'_adj ->
        app_opt (rel_opt (aggreg1 `Count n g))
    | op = parse_aggreg_adj; app_opt = parse_app_opt; n,g = parse_g'_adj -> app_opt (aggreg1 op n g) ]

and parse_ng1_count = dcg
    [ d1 = parse_ng1 -> (fun d -> a_number (bool1#and_ [d1; d])) ]

and parse_ng2 = dcg
    [ m1 = parse_modif_opt; p2 = parse_ng2_atom -> (fun x y -> bool0#and_ [m1 y; p2 x y]) ]
and parse_ng2_atom = dcg
    [ adj_opt = parse_p1_adj_opt; p2 = parse_p2_noun; app_opt = parse_app_opt -> (fun x -> app_opt (adj_opt (p2 x))) ]
(* creates ambiguities, e.g. "the average size of the doctors" (of all doctors OR of each doctor) *)
(*
    | op = parse_aggreg_noun; app_opt = parse_app_opt; lz,lr,y,r = parse_g2'_noun -> (fun x -> app_opt (aggreg op lz lr y (r x)))
    | op = parse_aggreg_adj; app_opt = parse_app_opt; lz,lr,y,r = parse_g2'_adj -> (fun x -> app_opt (aggreg op lz lr y (r x))) ]
*)

and parse_ng2_count = dcg
    [ p2 = parse_p2_count_noun -> (fun x d n ->
      bool0#if_true (d (Var "_"))
	(p2 x n)
	(bool0#fail "The individuals of 'numberOf' properties cannot be qualified"))
    | p2 = parse_ng2 -> (fun x d -> a_number (bool1#and_ [p2 x; d])) ]

and parse_app_opt = dcg "parse_app_opt"
  [ p1 = parse_app -> (fun d -> bool1#and_ [p1; d])
  |  -> bool1#id ]
and parse_app = dcg
  [ l = parse_user_var -> label l
  | e = parse_e_np -> unify e
  | patt = parse_pattern -> patt ]

and parse_rel_opt = dcg
    [ rel = parse_rel -> (fun d -> bool1#and_ [d; rel])
    |  -> bool1#id ]
and parse_rel = dcg
    [ rel = parse_bool bool1 parse_rel_atom -> rel ]
and parse_rel_atom = dcg
    [ rel = parse_brackets parse_rel -> rel
    | p1 = parse_p1_adj; cp = parse_cp -> init (fun x -> cp (p1 x))
    | p2 = parse_p2_adj; op = parse_op -> init (fun x -> op (p2 x))
    | _ = kwd "among"; npq = parse_npq_enum -> npq
    | _ = kwd "except"; npq = parse_npq_enum -> bool1#not_ npq
    | f1 = [ _ = kwd "with" -> bool1#id | _ = kwd "without" -> bool1#not_ ]; vp = parse_vp_have -> init (f1 vp)
    | _ = kwds ["that"; "which"; "who"]; vp = parse_vp -> init vp
    | _ = kwds ["that"; "which"; "who"; "whom"]; d = parse_relativize bool0 parse_s_for `None -> init d
(*
    | _ = kwds ["that"; "which"; "whom"]; np = parse_np; p2 = parse_p2_verb; cp = parse_cp ->
	init (fun x -> np (fun y -> cp (p2 y x)))
*)
    | _ = kwd "such"; _ = kwd "that"; s = parse_s_for -> init (fun x -> s)
    | np2 = parse_np2; _ = kwd "of"; _ = kwd "which"; vp = parse_vp ->
	init (fun x -> np2 x vp)
    | _ = kwd "whose"; p2 = parse_ng2;
      ( f1 = parse_aux kwds_be; np = parse_np ->
        init (fun x -> np (fun y -> p2 x y))
      | vp = parse_vp ->
        init (fun x -> exists (bool1#and_ [p2 x; vp])) )
    | mk = parse_marker; _ = kwd "which"; prep = parse_prep mk; s = parse_s_for ->
	init (fun z -> prep z s)
    | cmp, op = parse_comp_p2_unary parse_op -> (fun x -> op (fun y -> pred2 cmp x y))
    | op1, op2 = parse_comp_between parse_op -> (fun x -> op1 (fun x1 -> op2 (fun x2 -> bool0#and_ [pred2 pred2_geq x x1; pred2 pred2_leq x x2]))) ]

and parse_vp = dcg
    [ lvp = LIST1 parse_vp_pp SEP semicolon -> bool1#and_ lvp ]
and parse_vp_pp = dcg
    [ pp = parse_pp; vp = parse_vp_pp -> (fun x -> pp (vp x))
    | vp = parse_vp_bool -> vp ]
and parse_vp_bool = dcg
  [ vp = parse_bool bool1 parse_vp_aux -> vp ]
and parse_vp_aux = dcg
    [ vp = parse_brackets parse_vp_pp -> vp
    | f1 = parse_aux kwds_does; vp = parse_vp_atom -> f1 vp
    | f1 = parse_aux kwds_be; vp = parse_vp_be -> f1 vp
    | f1 = parse_aux kwds_have; vp = parse_vp_have -> f1 vp
    | vp = parse_vp_atom -> vp ]

and parse_aux roots = dcg
  [ _ = kwds roots; ( _ = kwd "not" -> bool1#not_ |  -> bool1#id )
  | _ = kwds (List.map (fun root -> root ^ "n") roots); "'t" -> bool1#not_ ]

and parse_vp_atom = dcg
    [ _ = share; vp = parse_vp_share -> vp
    | p1 = parse_p1_verb; cp = parse_cp -> (fun x -> cp (p1 x))
    | p2 = parse_p2_verb; op = parse_op -> (fun x -> op (p2 x)) ]

and parse_vp_be = dcg
  [ _ = kwd "there" -> bool1#true_
  | _ = kwd "the"; m1 = parse_modif -> m1
  | vp = parse_vp_be_bool -> vp
  | d = parse_npq_gen_atom ["which"] -> (fun x -> which d (unify x))
  | rel = parse_rel -> rel ]
and parse_vp_be_bool = dcg
  [ vp = parse_bool bool1 parse_vp_be_expr -> vp ]
and parse_vp_be_expr = dcg
  [ vp = parse_vp_be_atom -> vp
  | e1 = parse_expr parse_s_for bool0
      (dcg [ d = parse_vp_be_atom ->
	(fun k0 -> exists (bool1#and_ [d; k0])) ]) ->
      (fun x -> e1 (fun x1 -> unify x1 x)) ]
and parse_vp_be_atom = dcg
  [ d = parse_brackets parse_vp_be_bool -> d
  | d = parse_npq_gen_atom ["a"; "an"; "the"] -> d (* close_modif ? *)
  | _ = kwd "one"; _ = kwd "of"; npq = parse_npq_enum -> npq
  | _ = kwd "none"; _ = kwd "of"; npq = parse_npq_enum -> bool1#not_ npq ]
(*    | np = parse_np -> (fun x -> np (fun y -> unify x y)) ] (* this last case is sometimes problematic *) *)

and parse_vp_have = dcg
    [ npq2 = parse_npq2_gen ["which"]; rel_opt = parse_rel_opt ->
	(fun x -> which (rel_opt bool1#true_) (npq2 x))
    | np2 = parse_np2_void; rel_opt = parse_rel_opt ->
	(fun x -> np2 x (rel_opt bool1#true_))
    | p2 = parse_p2_noun; op = parse_op ->
	(fun x -> op (fun y -> p2 x y))
    | _ = a_the;
      ( cmp, p21, p22 = parse_comp_p2_binary parse_p2_noun parse_p2_noun ->
          (fun x -> kcomp2 (pred2 cmp) (p21 x) (p22 x))
      | cmp, p2, np = parse_comp_p2_binary parse_p2_noun parse_np_void ->
	  (fun x1 -> np (fun x2 -> bool0#and_ [diff x1 x2; kcomp2 (pred2 cmp) (p2 x1) (p2 x2)])) ) ]

and parse_vp_share = dcg "vp_share"
    [ det = parse_det; p2 = parse_ng2; _ = kwd "with"; np = parse_np_void ->
        (fun x1 -> det (p2 x1) (fun y -> np (fun x2 -> bool0#and_ [diff x1 x2; p2 x2 y]))) (* close_modif ? *)
    | _ = kwd "with"; np = parse_np; det = parse_det; p2 = parse_ng2 ->
	(fun x1 -> np (fun x2 -> bool0#and_ [diff x1 x2; det (p2 x1) (fun y -> p2 x2 y)])) ] (* close_modif ? *)

and parse_op = dcg
    [ pp = parse_pp; op = parse_op -> (fun d -> pp (op d))
    | op = parse_op_bool -> op ]
and parse_op_bool = dcg
    [ op = parse_bool bool1 parse_op_atom -> op ]
and parse_op_atom = dcg
    [ op = parse_brackets parse_op -> op
    | np = parse_np_void; cp = parse_cp -> (fun d -> np (fun y -> cp (d y))) ]

and parse_op_seq = dcg
    [ pp = parse_pp; opl = parse_op_seq -> (fun dl -> pp (opl dl))
    | opl = parse_op_seq_bool -> opl ]
and parse_op_seq_bool = dcg
    [ opl = parse_bool bool1 parse_op_seq_atom -> opl ]
and parse_op_seq_atom = dcg
    [ opl = parse_brackets parse_op_seq -> opl
    | npl = parse_seq parse_np; cp = parse_cp -> (fun dl -> npl (fun ly -> cp (dl ly))) ]

and parse_cp = dcg
    [ cp = parse_bool bool1 parse_cp_atom -> cp ]
and parse_cp_atom = dcg
    [ cp = parse_brackets parse_cp -> cp
    | pp = parse_pp; cp = parse_cp_atom -> (fun s -> pp (cp s))
    |  -> (fun s -> s) ]

and parse_pp = dcg
    [ pp = parse_bool bool1 parse_pp_atom -> pp ]
and parse_pp_atom = dcg
    [ pp = parse_brackets parse_pp -> pp
    | _ = kwd "how"; _ = [ _ = kwd "often" -> () | _ = kwd "many"; _ = kwd "times" -> () ] ->
      (fun s -> which bool1#true_ (how_often s))
    | mk = parse_marker;
      ( prep = parse_prep_opt mk; np = parse_np_void ->
	  (fun s -> np (fun z -> prep z s))
      | det = parse_det; prep = parse_prep mk; app_opt = parse_app_opt; rel_opt = parse_rel_opt ->
	  (fun s -> det (close_modif (app_opt (rel_opt bool1#true_))) (fun z -> prep z s))
      | _ = kwd "which"; prep = parse_prep mk; app_opt = parse_app_opt; rel_opt = parse_rel_opt -> (* = at Prep which thing Rel? *)
	  (fun s -> which (app_opt (rel_opt bool1#true_)) (fun z -> prep z s)) ) ]

and parse_prep_opt (marker : string) = dcg
    [ prep = parse_prep marker -> prep
    |  -> arg marker ]
and parse_prep (marker : string) = dcg
    [ op = parse_context marker -> context op
    | when "preposition marker should be 'at'" marker = "at"; uri = parse_uri re_word -> arg uri ]

and parse_g'_noun = dcg
    [ d = parse_g'_of; n, dims = parse_dims -> n, (fun y lz -> bool0#and_ [d y; dims y lz])
    | n, dims = parse_dims; d = parse_g'_of -> n, (fun y lz -> bool0#and_ [d y; dims y lz]) ]
and parse_g'_adj = dcg
    [ p1 = parse_ng1; n, dims = parse_dims -> n, (fun y lz -> bool0#and_ [p1 y; dims y lz])
    | p2 = parse_ng2;
      ( d = parse_g'_of; n, dims = parse_dims -> n, (fun y lz -> exists (fun x -> bool0#and_ [d x; p2 x y; dims x lz]))
      | n, dims = parse_dims; d = parse_g'_of -> n, (fun y lz -> exists (fun x -> bool0#and_ [d x; p2 x y; dims x lz])) ) ]
and parse_g'_of = dcg
    [ _ = kwd "of"; d1 = parse_npq_enum -> d1 ]

(*
and parse_g2'_noun = dcg (* TODO: add parse_per *)
    [ _ = kwd "of"; p2 = parse_ng2; y = new_var -> [], [], y, (fun x y -> p2 x y) ]
and parse_g2'_adj = dcg
    [ p2 = parse_ng2; y = new_var -> [], [], y, (fun x y -> p2 x y) ]
*)

and parse_dims = dcg
    [ _ = kwd "per"; dim = parse_dims_atom; n, dims = parse_dims_rest ->
       n+1, (fun y -> function z::lz -> bool0#and_ [dim y z; dims y lz] | _ -> assert false)
    |  -> 0, (fun y lz -> bool0#true_) ]
and parse_dims_rest = dcg
  [ _ = kwd "and"; n, dims = parse_dims -> n, dims
  |  -> 0, (fun y lz -> bool0#true_) ]
and parse_dims_atom = dcg
    [ p2 = parse_ng2 -> (fun y z -> p2 y z)
    | d = parse_npq_gen ["the"] -> (fun y z -> d z) ]

and parse_e_np = dcg [ ?ctx; x = ctx#parse_e_np -> x ]

and parse_p1_noun = dcg
  [ p1 = parse_bool bool1 parse_p1_noun_atom -> p1 ]
and parse_p1_noun_atom = dcg
  [ p1 = parse_brackets parse_p1_noun -> p1
  | ?ctx; p1 = ctx#parse_p1_noun -> p1 ]

and parse_p1_adj_opt = dcg
    [ p1_adj = parse_p1_adj -> (fun p1 -> bool1#and_ [p1; p1_adj])
    |  -> (fun p1 -> p1) ]
and parse_p1_adj = dcg
  [ p1 = parse_bool bool1 parse_p1_adj_atom -> p1 ]
and parse_p1_adj_atom = dcg
  [ p1 = parse_brackets parse_p1_adj -> p1
  | detn = parse_det_numeric; _ = OPT [ _ = kwds ["unit"; "units"] -> () ] ELSE (); pol, p2 = parse_p2_adj_measure ->
      (fun x -> detn pol (p2 x))
  | ?ctx; p1 = ctx#parse_p1_adj -> p1 ]

and parse_p1_verb = dcg
  [ p1 = parse_bool bool1 parse_p1_verb_atom -> p1 ]
and parse_p1_verb_atom = dcg
  [ p1 = parse_brackets parse_p1_verb -> p1
  | ?ctx; p1 = ctx#parse_p1_verb -> p1 ]

and parse_p1_imp = dcg [ ?ctx; p1 = ctx#parse_p1_imp -> p1 ]

and parse_pn_imp = dcg [ ?ctx; pn = ctx#parse_pn_imp -> pn ]

and parse_p2_noun = dcg
  [ p2 = parse_bool bool2 parse_p2_noun_atom -> p2 ]
and parse_p2_noun_atom = dcg
  [ p2 = parse_brackets parse_p2_noun -> p2
  | ?ctx; p2 = ctx#parse_p2_noun -> p2 ]

and parse_p2_count_noun = dcg [ ?ctx; p2 = ctx#parse_p2_count_noun -> p2 ]

and parse_p2_verb = dcg
  [ p2 = parse_bool bool2 parse_p2_verb_atom -> p2 ]
and parse_p2_verb_atom = dcg
  [ p2 = parse_brackets parse_p2_verb -> p2
  | ?ctx; p2 = ctx#parse_p2_verb -> p2 ]

and parse_p2_adj = dcg
  [ p2 = parse_bool bool2 parse_p2_adj_atom -> p2 ]
and parse_p2_adj_atom = dcg
  [ p2 = parse_brackets parse_p2_adj -> p2
  | p2, comp = parse_p2_adj_comp ->
      (fun x x' -> bool0#and_ [diff x x'; kcomp2 comp (p2 x) (p2 x')])
  | ?ctx; p2 = ctx#parse_p2_adj -> p2 ]
and parse_p2_adj_comp = dcg
  [ pol, p2 = parse_p2_adj_comparative; _ = kwd "than" ->
      p2, pred2 (pol pred2_gt) (*if pol then pred2 pred2_gt else pred2 pred2_lt*)
  | op = [ _ = kwd "more" -> pred2_gt | _ = kwd "less" -> pred2_lt | _ = kwd "as" -> pred2_eq ];
    pol, p2 = parse_p2_adj_measure; _ = kwds ["than"; "as"] ->
      p2, pred2 (pol op) (*if pol then pred2 op else bool2#inverse (pred2 op)*) ]

and parse_p2_adj_measure = dcg [ ?ctx; pol_p2 = ctx#parse_p2_adj_measure -> pol_p2 ]
and parse_p2_adj_comparative = dcg [ ?ctx; pol_p2 = ctx#parse_p2_adj_comparative -> pol_p2 ]
and parse_p2_adj_superlative = dcg [ ?ctx; pol_p2 = ctx#parse_p2_adj_superlative -> pol_p2 ]

and parse_marker = dcg [ ?ctx; mk = ctx#parse_marker -> mk ]
and parse_context marker = dcg [ ?ctx; op = ctx#parse_context marker -> op ]

and parse_aggreg_noun = dcg [ ?ctx; op = ctx#parse_aggreg_noun -> op ]
and parse_aggreg_adj = dcg [ ?ctx; op = ctx#parse_aggreg_adj -> op ]

and parse_modif_opt = dcg
    [ m1 = parse_modif -> m1
    |  -> bool1#true_ ]
and parse_modif = dcg
  [ m1 = parse_modif_order_offset_limit -> m1 ]
and parse_modif_order_offset_limit = dcg
  [ "decreasing" -> (fun x -> open_modif (`Mod { modif_default with order=`DESC }) [] x bool0#true_)
  | "increasing" -> (fun x -> open_modif (`Mod { modif_default with order=`ASC }) [] x bool0#true_)
  | op_modif = parse_offset_limit;
    ( m1 = parse_order op_modif -> m1
    |  -> (fun x -> open_modif (`Mod op_modif) [] x bool0#true_) )
  | m1 = parse_order { modif_default with limit=1 } -> m1 ]
and parse_offset_limit_opt = dcg
  [ op_modif = parse_offset_limit -> op_modif
  |  -> { modif_default with limit=1 } ]
and parse_offset_limit = dcg
  [ limit = parse_nat -> { modif_default with limit=limit }
  | ord1 = parse_ordinal;
    ( _ = kwd "to";
      ( ord2 = parse_ordinal when "invalid modifier range" ord2 > ord1 -> { modif_default with offset=ord1-1; limit=ord2-ord1+1 }
      | _ = kwd "last" -> { modif_default with offset=ord1-1 } )
    |  -> { modif_default with offset=ord1-1; limit=1 } ) ]
and parse_order op_modif = dcg
  [ pol, p2 = parse_p2_adj_superlative ->
      (fun x -> exists (fun y -> open_modif (pol (`Mod { op_modif with order=`DESC })) [x] y (p2 x y)))
  | _ = kwd "first" -> (fun x -> open_modif (`Mod op_modif) [] x bool0#true_)
  | _ = kwds ["greatest"; "highest"; "latest"] -> (fun x -> open_modif (`Mod { op_modif with order=`DESC }) [] x bool0#true_)
  | _ = kwds ["least"; "lowest"; "earliest"] -> (fun x -> open_modif (`Mod { op_modif with order=`ASC }) [] x bool0#true_) ]
